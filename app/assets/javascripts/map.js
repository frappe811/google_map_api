$(function() {
  getLocation.initialize();
});

var getLocation = {
   initialize: function() {
     getLocation.fetch_data();
   },

   fetch_data: function() {
     var url_path = "http://localhost:3000/api/v1/drivers/1";
     $.get(url_path, function(data) {
       const lat = data['lat'];
       const long = data['long'];
       getLocation.draw_map(lat, long);
       console.log(data)
     }, 'json');
   },

   draw_map: function(lat, long) {

     var companyLatlong = new google.maps.LatLng(lat, long);
     var mapOptions = {
       zoom: 15,
       center: companyLatlong,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
     var map = new google.maps.Map(document.getElementById('map'), mapOptions);
     var marker = new google.maps.Marker({
       position: companyLatlong,
       map: map
     });
   }
}
